import csv

csv_file = input('Enter the name of the csv file: ')
txt_file = input('Enter the name of the txt output file: ')


#with open(txt_file, 'w') as output_file:
#	with open(csv_file, 'r') as input_file:
#		[output_file.write("new Pose2d(" + ",".join(row) + ",new Rotation2d(" + '' + '),' + '\n') for row in csv.reader(input_file)]
#	output_file.close()

with open(txt_file, 'w') as output_file:
	with open(csv_file, 'r') as input_file:
		[output_file.write("new Pose2d(" + row[0] + "," + row[1] + ",new Rotation2d(" + row[2] + ')),' + '\n') for row in csv.reader(input_file)]
	output_file.close()